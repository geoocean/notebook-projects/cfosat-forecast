#!/usr/bin/env python
# coding: utf-8

# In[1]:


import sys
import os
import os.path as op

import numpy as np
import pandas as pd
import datetime
import netCDF4 as netcdf
from datetime import timedelta

import matplotlib as mpl
import matplotlib.pyplot as plt
from math import pi
import xarray as xr
import glob
import math
from geopy.distance import geodesic
from scipy.interpolate import griddata

from IPython.display import HTML

# dev library
import sys
sys.path.insert(0, op.join(os.path.abspath(''), '..'))

# dependencies
from lib import wrapSpectra, spectra, sat, plot_sat, model, fireworks, plot_storms
from lib.config import * 


# # Swell Field reconstruction

# Storm source identification

# **Satellite tracks data - Mfwam model data**

# In[2]:


# coords: (file, nbox, posneg), vars: (longitude, latitude, time)
ds_data = xr.open_dataset(op.join(p_output, 'ds_dataTracks.nc'))
ds_ww3 = xr.open_dataset(op.join(p_output, 'ww3_wind.nc'))


# ## CFOSAT Partitions information

# Peak period and Direction of propagation given by the CFOSAT partitions information

# In[3]:


#wrapSpectra.CFOSAT_partitions(ds_data, beam, min_wavelength, max_wavelength)


# In[4]:


# xr.Dataset() Coords: (nbox, partition, posneg, file), Data: (SWH(m), wavelength(m), direction (°))
ds = xr.open_mfdataset(glob.glob(op.join(p_output, 'process_CFOSATparts', '*')),
                       engine='netcdf4', combine='nested', concat_dim='file', parallel=True)


# In[5]:


# From ds_data and ds - > nbox, nadir, file = f (longitude, latitude)
ds = ds.load()
ds_input = xr.merge([ds_data, ds])
ds_input['Tp'] = np.sqrt(2*np.pi*ds_input['wavelength(m)']/g)
ds_input = ds_input.where((ds_input.Tp >= 16) & (ds_input['SWH(m)'] > 0.5), drop=True)
ds_input = ds_input.rename({'direction (°)':'dir', 'SWH(m)':'hs', 'wavelength(m)':'wl'})


# In[6]:


ds_input = ds_input.to_dataframe().reset_index().dropna()


# ## Retro-propagation

# All observations past-positions are calculated following geodesics at group velocity in the direction opposite to the direction of propagation until land is reached and for a maximum time of 14 days. Group velocity is estimated from dominant wavelength using linear dispersion relation, valid for swell with small steepness. (Romain)
# 
# Along this past trajectory, the fact that the generation region is detected depends on two conditions. First, a minimum number of swell observations have to converge to this region. This condition is referred to as refocusing and it is further detailed afterwards. Second, the local sea surface wind speed has to reach a threshold value compatible with the swell wavelength. Indeed, a wave of peak period Tp requires a threshold wind speed Umin
# around 0.12gTp to be generated, where g is the Earth gravity. The auxiliary wind speed is given by the WW3 10m wind speed.
# 
# In practice, because of the inability of input wind data to describe the highest wind speed range, we consider a limit wind speed equal to two-thirds of Umin. Thanks to this wind speed threshold, cross sea regions occurring under calm wind conditions are rightfully rejected from the following storm detection.

# In[7]:


folder = 'fireworks_PARTITIONS'
#fireworks.calculate_fireworks(ds_input, ds_ww3, folder)


# In[8]:


dst = xr.open_mfdataset(glob.glob(op.join(p_output, folder, '*')),
                       engine='netcdf4', combine='nested', concat_dim='case', parallel=True)


# ## Refocusing regions

# **Romain Husson 2012**

# It is based on the analysis of retro-propagated swell density-maps over time. These densitymaps are 2D histograms indicating, for a given time step, the number of retro-propagated observations whose threshold wind speed condition is fulfilled within each cell. The density-map geographical extension covers all longitudes, and goes from 74◦S to 74◦N with 2 2◦ cells. This resolution is justified by the limited precision on the swell retro-propagated observations’ location, caused by the error on the swell peak period and direction measurements and the resulting mis-positioning after retro-propagation. In order to be able to compare density-map cells at different latitudes, each cell value is normalized by the cell surface.

# **Persistency in time (24 hours)** <br>
# **Density-maps at successive time steps showing the refocusing
# region displacement**<br>
# **Select sparce observations in time and space**

# In[9]:


init = pd.Timestamp(dst.date.values.min()).round('D')
end = pd.Timestamp(dst.date.values.max()).round('D')
time = np.arange(init, end, datetime.timedelta(hours=12))[::-1]

# settings
dxlon = 2
dylat = 2
xedges = np.arange(-180, 180 + 2*dxlon, dxlon)
yedges = np.arange(-90, 90 + 2*dylat, dylat)


# ## Refocusing 2D density maps

# ### The density map analysis follows an iterative process and the time series are inspected going backward in time

# In[10]:


max_density = 0
for pt, tm in enumerate(time[:-1]):

    ds_delta = dst.sel(date=slice(time[pt+1], tm))
    dataframe = ds_delta.to_dataframe().reset_index().dropna()

    # histogram 2D
    Z, xedges, yedges = np.histogram2d(dataframe.lon, dataframe.lat, bins=(xedges, yedges))
    
    # calculate maxima and preserve the closest storm in time
    if Z.max() > max_density:
        max_density = Z.max()
        Tmax = tm
        Pmax = pt
        


# In[11]:


# Minimun number of observations 
th = 4
cmap = plot_storms.colormap(int(max_density), th)


# In[12]:


time = time[::-1]


# **In Tmax (maximun density), a watershed algorith is applied to identify storms sub-peacks**
dsm_contour = dst.sel(date=slice(time[Pmax+1], Tmax))
dfm_contour = dsm_contour.to_dataframe().reset_index().dropna()

# histogram 2D
Z, xed, yed = np.histogram2d(dfm_contour.lon, dfm_contour.lat, bins=(xedges, yedges))

cs = plt.contour(xed[:-1], yed[:-1], Z.T, levels = [0],
                 #transform=ccrs.PlateCarree()
                )
contours = cs.collections[0].get_paths()

# ## Storms

# In[13]:


ani = plot_storms.animate_storms(dst, ds_input, ds_ww3, Tmax, max_density, time, cmap, xedges, yedges)


# In[14]:


HTML(ani.to_jshtml())


# In[15]:


sys.exit()


# ## Storms + Wind

# In[16]:


ani = plot_storms.animate_wind_storms(dst, ds_input, ds_ww3, Tmax, max_density, time, cmap, xedges, yedges)


# In[17]:


HTML(ani.to_jshtml())


# ## Back-propagated trayectories - wind condition

# In[13]:


ani = plot_storms.animate_backtracks(dst, ds_input, ds_ww3, Tmax, max_density, time, cmap, xedges, yedges)


# In[14]:


HTML(ani.to_jshtml())


# In[ ]:




