#!/usr/bin/env python
# coding: utf-8

# In[1]:


import sys
import os
import os.path as op

import numpy as np
import pandas as pd
import datetime
import netCDF4 as netcdf
from datetime import timedelta
from math import pi
import xarray as xr
import glob

# dev library
import sys
sys.path.insert(0, op.join(os.path.abspath(''), '..'))

# dependencies
from lib import wrapSpectra, spectra, sat, plot_sat, model
from lib.config import * 


# # Storage energy bins
Filters:
    - In the total energy captured by the satellite -> ci = 0.5 * np.std(diff)/np.mean(diff)
# In[2]:


ds_data = xr.open_dataset(op.join(p_output, 'ds_dataTracks.nc'))


# In[3]:


for pfile, file in enumerate(ds_data.file.values):
    if pfile >= 140:
        try:
            sys.stdout.write('\r File number: {0}\{1}'.format(pfile,len(ds_data.file.values)))
            sys.stdout.flush()

            dsf = ds_data.isel(file=pfile).dropna(dim='nbox')
            ds_params_freqs = wrapSpectra.compute_spectra(dsf, file, beam,
                                                          min_wavelength, max_wavelength)
            ds_params_freqs.to_netcdf(op.join(p_output, 'process_FilesFreq', 'F{0}_bins.nc'.format(pfile)))

        except: continue


# In[4]:


ds = xr.open_mfdataset(glob.glob(op.join(p_output, 'process_FilesFreq', '*')),
                       engine='netcdf4', combine='nested', concat_dim='file', parallel=True)
ds.to_netcdf(op.join(p_output, 'energy_BINS.nc'))


# In[5]:


ds


# In[ ]:




