#!/usr/bin/env python
# coding: utf-8

# In[1]:


# sys
import os
import os.path as op
import sys
import glob

# basics
import numpy as np
import pandas as pd
import xarray as xr
import datetime
from matplotlib import pyplot as plt
from matplotlib.animation import FuncAnimation
from matplotlib import pyplot as plt, animation
from mpl_toolkits.axes_grid1 import make_axes_locatable
import cmocean 

from IPython.display import HTML, display

# cartopy maps
import cartopy.crs as ccrs
from cartopy.io.shapereader import Reader
from cartopy.feature import ShapelyFeature

# dev library
import sys
sys.path.insert(0, op.join(os.path.abspath(''), '..'))

# dependencies
from lib import spectra, plot_spectra
from lib.config import *

# warnings
import warnings
warnings.filterwarnings("ignore")


# # Animation Satellite Model

# In[2]:


# Load satellite tracks
ds_data = xr.open_dataset(op.join(p_output, 'ds_dataTracks.nc'))
df_data = ds_data.to_dataframe().dropna().sort_values('time').reset_index()


# In[3]:


# animation settings
df_data = df_data.loc[df_data.posneg==0].reset_index(drop=True)
extra = [int(i) for i in np.linspace(0, len(df_data)-1, 1000)]
dff_data = df_data.iloc[extra]


# ## Load WW3 model vars Hs, Tp, Dir

# In[ ]:


# Load model data
files = glob.glob(op.join(ww3_path, '*'))
#var = ['pdir0', 'pdir1', 'pdir2', 'pdir3', 'pdir4', 'pdir5',
#       'ptp0','ptp1','ptp2','ptp3', 'ptp4', 'ptp5']
var = ['uwnd', 'vwnd']
#name, time = [], []
ds_ww3 = xr.Dataset()

for p, i in enumerate(files):
    date = files[p].split('WW3-GLOB-30M_')[1][:-3]
    date = np.datetime64(pd.to_datetime(date))
    #if (date > forecast_day - datetime.timedelta(days=10)) & (date <= forecast_day):
    if (date > forecast_day - datetime.timedelta(days=20)) & (date <= forecast_day - datetime.timedelta(days=10)):
        dst = xr.open_dataset(i)[var]
        ds_ww3 = xr.merge([ds_ww3, dst])
        #name.append(i)
        #time.append(date)

#wind = np.sqrt(U10**2+V10**2)


# In[6]:


ds_ww3['wind'] = np.sqrt(ds_ww3.uwnd**2 + ds_ww3.vwnd**2)


# In[7]:


ds_ww3.to_netcdf(op.join(p_output, 'ww3_wind2.nc'))


# In[4]:


ds1 = xr.open_dataset(op.join(p_output, 'ww3_wind1.nc'))
ds2 = xr.open_dataset(op.join(p_output, 'ww3_wind2.nc'))


# In[6]:


ds2


# In[12]:


ds3 = xr.combine_by_coords([ds1, ds2])
ds3.to_netcdf(op.join(p_output, 'ww3_wind.nc'))


# In[13]:


ds3


# In[7]:


ds3


# ## Animate

# In[8]:


# low resolution shapefile
shape_feature_l = ShapelyFeature(Reader(fname_l).geometries(),
                                 ccrs.PlateCarree(),
                                 facecolor='beige', alpha=1, edgecolor='black')


# In[9]:


def addmap(ax):
    'Cartopy map customize'
    ax.patch.set_facecolor('cornflowerblue')
    ax.patch.set_alpha(0.7)
    ax.add_feature(shape_feature_l)


# In[10]:


def find_time(date, time_array):
    return time_array[np.argmin(np.abs(time_array-date))]


# In[11]:


dff_data = dff_data[260:]


# In[12]:


def animate(dff_data, var, cmap):
    
    # Get a handle on the figure and the axes
    fig = plt.figure(figsize=(15,10))
    ax = fig.add_subplot(111, projection=ccrs.PlateCarree())#central_longitude=180)
    addmap(ax)
    
    time_sat = find_time(dff_data.time.values[0], ds_ww3.time.values)
    
    # Plot the initial frame. 
    cax = ds_ww3[var].sel(time=time_sat).plot(
        add_colorbar=False,
        cmap=cmap,
    )

    scat = ax.scatter(dff_data.longitude.values[0], 
                      dff_data.latitude.values[0],
                      c='k',s=15,
                      transform=ccrs.PlateCarree())
    
    # plt colorbar
    divider = make_axes_locatable(ax)
    ax1 = divider.new_horizontal(size="3%", pad=0.1, axes_class=plt.Axes)
    fig.add_axes(ax1)
    plt.colorbar(cax, cax=ax1, label='Hs(m)')
    
    # Next we need to create a function that updates the values for the colormesh, as well as the title.
    def animate(frame):

        time_sat = find_time(dff_data.time.values[frame], ds_ww3.time.values)
        scat = ax.scatter(dff_data.longitude.values[frame], 
                          dff_data.latitude.values[frame], c='k', s=15)
        
        cax.set_array(ds_ww3[var].sel(time=time_sat).values.flatten())
        ax.set_title("Time = " + str(dff_data.time.values[frame]))
                   
    # Finally, we use the animation module to create the animation.
    ani = animation.FuncAnimation(
        fig,             # figure
        animate,         # name of the function above
        frames=700,       # Could also be iterable or list
        interval=200     # ms between frames
    )
    plt.close()
    return(ani, fig)


# In[16]:


ani0, fig0 = animate(dff_data, 'hs', 'rainbow')
HTML(ani0.to_jshtml())


# In[ ]:




