# CFOSAT-based Swell Forecast System

A set of notebooks comprises a satellite data-driven methodology to evaluate the energy arrival at any particular ocean location worlwide

![Methodology](skt.png)

Figure. a) Workflow of the CFOSAT-Forecast System methodology, b) Travel source evaluation, c) CFOSAT observations selected after temporal and spatial masks, d) measured significant wave heigh of energy bins before propagation