#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import os.path as op
import sys
import glob

import numpy as np
import pandas as pd
import netCDF4 as netcdf
import datetime
import xarray as xr
from math import pi
import math
from datetime import timedelta

import gc

import warnings
warnings.filterwarnings('ignore')

# dependencies
from lib import spectra, io
from lib.config import *

def mfwam_info(SWIM_L2_NC_file_name, date, nadir_side, lon_coor, lat_coor):
    'Find partitions information in MFWAN datarmor files'
    
    date64 = datetime.datetime.strptime(date, "%Y-%m-%dT%H:%M:%S")
    dayYear = date64.strftime('%j')

    mfwam_data = pd.DataFrame()
    for dayYear in [str(int(dayYear)-1).zfill(3), dayYear, str(int(dayYear)+1).zfill(3)]:
        file2 = os.listdir(op.join(mfawm, '2021', dayYear))[0]
        mfwam_data = pd.concat([mfwam_data, pd.read_csv(
                op.join(mfawm, '2021', dayYear, file2),
                delimiter='\s+'
            )])
    try:
        mfwam_sel = mfwam_data.loc[(mfwam_data.L2name == SWIM_L2_NC_file_name)]
        min_point, min_dist = spectra.get_box(mfwam_sel.lon.values, mfwam_sel.lat.values, lon_coor, lat_coor)
        mfwam_sel = mfwam_sel.iloc[[min_point]]

        return(mfwam_sel)
    
    except:
        return(pd.DataFrame())

def duplicate_spectra(cdf_i, n_beam_l2):
    # slope density
    p_comb = cdf_i['pp_mean'].isel(n_beam_l2=n_beam_l2)

    p_mask0 = cdf_i['mask'].isel(n_beam_l2=n_beam_l2, npartitions=0)
    p_mask1 = cdf_i['mask'].isel(n_beam_l2=n_beam_l2, npartitions=1)
    p_mask2 = cdf_i['mask'].isel(n_beam_l2=n_beam_l2, npartitions=2)

    # origial slope density spectra 
    ds_Slope = spectra.SlopeSpectra(cdf_i, p_comb)

    # 360º partition-masks
    ds_mask0 = spectra.SlopeSpectra(cdf_i, p_mask0)
    ds_mask1 = spectra.SlopeSpectra(cdf_i, p_mask1)
    ds_mask2 = spectra.SlopeSpectra(cdf_i, p_mask2)
    ds_mask = xr.concat([ds_mask0, ds_mask1, ds_mask2], dim='partition')
    
    return(ds_Slope, ds_mask)

def conserv_energy(ds0, ds1):
    'Function to determine the conservation of energy between original and downscale spectra'
    factor = ds0.slope.sum()/ds1.slope.sum()
    ds1['slope'] = ds1['slope'] * factor
    return(ds1)

def compute_spectra(cdf, SWIM_L2_NC_file_name):
    """
    Computes the slope density spectra
    """
    
    # Initialization
    min_wavelength = cdf.wlmin
    max_wavelength = cdf.wlmax
    dphi = cdf.dphi
    #n_beam_l2 = int((beam-6)/2)
    
    array = []
    
    for pbox, box in enumerate(cdf['nbox'].values):
        
        for posneg in [0, 1]:
            
            cdf_i = cdf.isel(n_beam_sig0=5, n_posneg=posneg, n_box=pbox, n_tim=0)

            if not np.isnan(cdf_i['time_spec_l2']):
                
                lon_coor, lat_coor = cdf_i['lon_spec_l2'], cdf_i['lat_spec_l2']
                date = (t0 + datetime.timedelta(seconds=float(cdf_i['time_spec_l2']))).strftime('%Y-%m-%dT%H:%M:%S')

                # Wave parameters
                swh, pwl, pdir = cdf_i.isel(n_beam_l2=n_beam_l2)['wave_param_part_combined'].values
                
                # Get 0-360º spectra 
                ds_Slope, ds_mask = duplicate_spectra(cdf_i, n_beam_l2)

                try:
                    # MFWAM partitions
                    mfwam_sel = mfwam_info(SWIM_L2_NC_file_name, date, posneg, lon_coor, lat_coor)
                
                    # Remove the directional ambiguity
                    mask_dir = dir_ambiguity(pdir, pwl, mfwam_sel, lon_coor, lat_coor, swh)

                    # firewall: there are no matching partitions
                    if not np.isnan(mask_dir).all():

                        dirs = ds_Slope.dp.values
                        ds_Slope_sp = spectra.remove_amb(cdf_i, ds_Slope, ds_mask, mask_dir)
                        ds_Slope_s = ds_Slope_sp.sum(dim='partition')
                        
                        # Resample spectra from 15º to 5º
                        ds0 = xr.Dataset({'slope':(('dp', 'k'), ds_Slope_s.sel(dp=0).slope.values.reshape(1,-1))},
                                    coords={'dp': [360], 'k': ds_Slope_s.k.values})

                        ds1 = xr.combine_by_coords([ds_Slope_s, ds0])

                        # Resample directional spectra from 15º to 5º
                        ds_Slope1 = ds1.interp(dp=np.arange(0, 360, delta_dir), method="linear")
                        
                        # Resample frequential spectra from 15º to 5º
                        L = np.linspace(20, 500, 50)
                        ds_Slope1 = ds_Slope1.interp(k=(2*np.pi/L)[::-1], method="linear")

                        # conserve energy
                        #ds_Slope = conserv_energy(ds_Slope_s, ds_Slope1)
                        
                        # wind var
                        w = np.sqrt(cdf_i['u10_ecmwf']**2 + cdf_i['v10_ecmwf']**2)

                        distance, bearing_p1, bearing_p2 = spectra.geoBearing(lon_coor, lat_coor)

                        # Select sector and compute mean parameters
                        val = ds_Slope1.where(ds_Slope1.dp==ds_Slope1.dp[np.argmin(np.abs(ds_Slope1.dp.values - bearing_p2))])
                        ds_sector = val.assign(file=[SWIM_L2_NC_file_name], posneg=[posneg], nbox=[box])
                        ds_sector = ds_sector.assign(
                            {
                                'bearing_p1':(('file', 'nbox', 'posneg'), np.array(bearing_p1).reshape(1,1,-1)),
                                'bearing_p2':(('file', 'nbox', 'posneg'), np.array(bearing_p2).reshape(1,1,-1)),
                                'distance':(('file', 'nbox', 'posneg'), np.array(distance).reshape(1,1,-1)),
                                'w':(('file', 'nbox', 'posneg'), np.array(w).reshape(1,1,-1))
                            })
                        array.append(ds_sector)
                except:
                    print('Box {0} & Nadir {1} breaks in MFWAM association'.format(box, posneg))
                    
    dataset = xr.combine_by_coords(array)
    return(dataset)

def dir_ambiguity(dom, dom_wl, ds_mfwam, lon_coor, lat_coor, swh):
    '''
    Function to remove the 180º direction ambiguity considering the freq-dir energy peak of WW3 partitions
    Wave partitions are not neccesarilly ordered by decreasing energy TODO
    '''
    dom_dir = dom.copy()
    mdirs =  [ds_mfwam.mdww.values, ds_mfwam.mwd1.values, ds_mfwam.mwd2.values]
    mtms = [ds_mfwam.mpww.values, ds_mfwam.mwp1.values, ds_mfwam.mwp2.values]
    
    # From WW3 partitions, find the most similar value in SWIM
    # define a threshold for k and dir
    poswims, vector_p = [], list(np.ones(len(mdirs))*np.nan)
    for pang, ang in enumerate(mdirs):
        if np.isnan(ang) == False: # ww3 found partition
            
            success, pos_swim, flip = pos_find(dom, dom_wl, mdirs[pang], mtms[pang]) # we found a SWIM associated partition 
            if success: vector_p[pang] = pos_swim # save the correspondance between MFWAN and CFOSAT partitions
            if pos_swim not in poswims: # if the partition has not been previosly selected
                
                poswims.append(pos_swim)
                if (success) & (flip==False): dom_dir[pos_swim] = mtms[pang]
                elif (success) & (flip): dom_dir[pos_swim] = dom_dir[pos_swim] + 180
    
    # positions not found
    posnans = [i for i in range(len(dom_dir)) if i not in poswims]
    dom_dir[posnans], swh[posnans] = np.nan, np.nan

    dom_dir = set_outlier(ds_mfwam, dom_dir, swh, vector_p)

    return(dom_dir)

def set_outlier(mfwam_sel, dom, swh, vector_p):
    'Set as outlier if SWH differences between MFWAM and CFOSAT is bigger than x meters'
    mhs = [mfwam_sel.swh.values, mfwam_sel.swh1.values, mfwam_sel.swh2.values]
    for part_cfosat, part_mfwam in enumerate(vector_p):
        if not np.isnan(part_mfwam):
            swh_cfosat = swh[part_cfosat]
            swh_mfwam = mhs[part_mfwam]
            
            perc = swh_cfosat/swh_mfwam
            
            if perc < flag_hs: dom[part_cfosat] = np.nan
    
    return(dom)

def pos_find(dom, dom_wlg, dirw, tpw):
    """
    Define thresholds (Wavelength, wavenumber and direction) and look for similar values
        Args:
            dom -     SWIM part direction
            dom_wlg - SWIM part wavelength
            dirw -    MFWAM dirs
            tpw -     MFWAM tps
            
        Returns:
        
    """
    
    thL = 90
    thk = 2*np.pi/thL
    thd = 35
    
    # from Tp to L
    lpw = (g*tpw**2)/(2*np.pi)

    # First filter with wavelength
    for p, wl in enumerate(dom_wlg):
  
        minl = np.abs(wl - lpw) 

        if minl <= thL:
            
            # angular difference
            value = dom[p]
            
            # 0-180º angular sector and complementary
            mind = np.abs(get_distance(value, dirw))
            mind1 = np.abs(get_distance(value+180, dirw))
            
            if mind <= thd:
                return(True, p, False)
            elif mind1 <= thd:
                return(True, p, True)

    return(False, np.nan, False)

def get_distance(unit1, unit2):
    phi = abs(unit2-unit1) % 360
    sign = 1
    # used to calculate sign
    if not ((unit1-unit2 >= 0 and unit1-unit2 <= 180) or (
            unit1-unit2 <= -180 and unit1-unit2 >= -360)):
        sign = -1
    if phi > 180:
        result = 360-phi
    else:
        result = phi

    return result*sign
