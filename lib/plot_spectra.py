#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
plot_spectre_2D_casys.py
:author: A. Jouzeau, M. Dalila
:creation date : 09-10-2017
:last modified : 08-11-2019

python 2.7 test OK python 3.X pas de test
Copyright 2017, CLS.

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
General Public License for more details (http://www.gnu.org/licenses/).
"""
import os
import os.path as op
import sys

from math import pi
import numpy as np
import argparse
import netCDF4 as netcdf
import datetime
import xarray as xr
import pandas as pd
from scipy.interpolate import griddata

from matplotlib import pyplot as plt
from matplotlib.patches import Patch
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import MaxNLocator

# cartopy maps
import cartopy.crs as ccrs
from cartopy.io.shapereader import Reader
from cartopy.feature import ShapelyFeature

from lib import plot_sat
from .config import *


def pol2cart(rho, phi):
    """
    Polar => Carthesian
    Usage:
        (x,y) = pol2cart(rho, phi)
    With:
        :param      rho     :distance to center     :NA     :NA     :E
        :param      phi     :angle                  :NA     :NA     :E
        :type       rho     :float
        :returns    (x,y)   :coordinates in carthesian
        :rtype      (x,y)   :tuple of floats
    """
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return(x, y)

def plot_polarfig(mask, ds_color, cdf_i, ax=None, fig=None, vmin=None, vmax=None, lmin=None, lmax=None, disp_partitions=True):
   
    if ax is None:
        fig, ax = plt.subplots(facecolor='white', figsize=(11, 9))
    else:
        fig = plt.gcf()

    title = '{0}\nCoordinates: {1:.2f}, {2:.2f}\nDate: {3}'.format(cdf_i.title, cdf_i['lon_spec_l2'].values, cdf_i['lat_spec_l2'].values, cdf_i.date_created)
    
    ds_color = ds_color.transpose('k', 'dp')
    mask = mask.transpose('partition','k', 'dp')
    
    # size of wave number vector
    n_k = len(ds_color.k)
    n_phi = len(mask.dp)
    
    # polar coordinates
    theta = np.deg2rad(ds_color.dp.values)
    theta1 = np.append(theta, theta[0]) 
    
    # Y-axis from wavenumber k to wavelength L
    yL = 2*np.pi/ds_color.k.values
    
    # Spectrum map
    z = np.ma.filled(ds_color.slope, fill_value=np.nan)
    z1 = np.column_stack((z[:,:],z[:,-1]))
    pc = ax.pcolormesh(theta1, yL, z1,
                       cmap='rainbow', vmin=vmin, vmax=vmax, zorder=1) 
    
    ax.set_theta_offset(np.pi/2)
    ax.set_theta_direction(-1)
    ax.yaxis.set_major_locator(MaxNLocator(4)) 
    ax.set_rlabel_position(45)
    ax.yaxis.set_major_locator(MaxNLocator(4)) 

    # radius labels in white
    ax.tick_params(labelcolor='k', axis='y')
    ax.set_rlim(bottom=lmax, top=lmin)

    # add Partitions edges
    colors, zorders, linewidths = ['#FF0E00', 'b', 'w'], [2, 3, 4], [2, 1, 2]
    if disp_partitions:
        for ppart, part in enumerate(mask.partition):
            theta = np.deg2rad(mask.dp.values)
            theta1 = np.append(theta, theta[0]) 
            
            z = mask.sel(partition=part).slope.values
            z1 = np.column_stack((z[:,:], z[:,-1]))

            ax.contour(theta1, yL, z1,
                       levels=[0.5],
                       #alpha=0.2,
                       colors=colors[ppart],
                       origin='upper',
                       linewidths=linewidths[ppart],
                       linestyles='dashed',
                       label='partition {0}'.format(ppart),
                       zorder=zorders[ppart])
            
    #ax.yaxis.set_tick_params(fontweight='bold')
    ax.set_title(title)
    ax.grid(False)
    
    return(pc)

def plotSpectra(fig, axt, cdf_i, ds_slope, ds_mask, vmin, vmax, lmin, lmax, partitions):
    
    kspectra = cdf_i['k_spectra']
    
    pc = plot_polarfig(ds_mask, ds_slope, cdf_i, ax=axt, fig=None, vmin=vmin, vmax=vmax, lmin=lmin, lmax=lmax, disp_partitions=partitions)
    
    if partitions:
        p1 = Patch(edgecolor='#FF0E00',linestyle='dashed',
                       linewidth=1.0, fill=False)
        p2 = Patch(edgecolor='b', linestyle='solid',
                       linewidth=1.0, fill=False)
        p3 = Patch(edgecolor='w',linestyle='dashdot',
                           linewidth=1.0, fill=False)

        legend = axt.legend([p1, p2, p3],
                           ['Partition 1', 'Partition 2', 'Partition 3'],
                           loc='best')

        frame = legend.get_frame()
        frame.set_facecolor('black')
        for text in legend.get_texts():
            plt.setp(text, color = 'w') 

    return(fig, axt)

def plotModel(fig, axt, spec, cmap, vmin, vmax, kmin, kmax):
    'Plot polar spectra form WW3 model. Convert direction from going TO to coming FROM'

    title = '2D slope spectrum WW3'
    suptitle = '\nCoordinates: {0:.2f}°, {1:.2f}°\nDate: {2}'.format(spec.lon.values, spec.lat.values, spec.time.values)
    title += suptitle

    # eliminate row,cell dimensions
    x = spec.direction
    y = spec.frequency
    z_density = spec.ww3_efth
    
    # 
    z_slope = z_density * 2*np.pi/(9.806/(2*np.pi*spec.frequency**2)) # Z slope = Z density * k
    
    # frequencies: center frequency bin
    
    # Y-axis from wavenumber k to wavelength L
    yL = g/(2*np.pi*y**2)
    
    # Flip direction
    xc = []
    for pd, dirt in enumerate(x):
        if dirt < 180: xc.append(dirt + 180)
        else: xc.append(dirt - 180)
    
    xc = np.deg2rad(xc)
    xc = np.append(xc, xc[0])
    z1 = np.column_stack((z_slope[:,:],z_slope[:,-1]))
    
    pc = axt.pcolormesh(xc, yL, np.sqrt(z1), vmin=vmin, vmax=vmax) 
    
    #axt.set_rlim(kmin,kmax)
    axt.set_rlim(bottom=kmax, top=kmin)
    pc.set_cmap(cmap)
    axt.set_title(title)
    axt.set_theta_offset(np.pi/2)
    axt.set_theta_direction(-1)
    axt.yaxis.set_major_locator(MaxNLocator(4)) 

    # add colorbar
    #axc, loc = plot_sat.add_colorbar(axt)
    axc = fig.add_axes([1, 0.3, 0.025, 0.4])
    fig.add_axes(axc)
    cbar = fig.colorbar(pc, cax=axc)
    
    return(fig, axt)


def plotPartitions(fig, gs, wave_spec, mask_comb, K_SPECTRA, 
                  box, posneg,
                  N_PHI, k_min, k_max,
                  vmin, vmax, partitions,
                  lon_coor, lat_coor, date):

    title = '2D mean slope spectrum, for box: ' + str(box) + ', posneg: '+ str(posneg)
    suptitle = '\nCoordinates: {0:.2f}°, {1:.2f}°\nDate: {2}'.format(lon_coor, lat_coor, date)
    title += suptitle
    
    p1 = Patch(edgecolor='#FF0E00',linestyle='dashed',
                   linewidth=1.0, fill=False)
    p2 = Patch(edgecolor='k', linestyle='solid',
                   linewidth=1.0, fill=False)
    p3 = Patch(edgecolor='#D4FF9C',linestyle='dashdot',
                       linewidth=1.0, fill=False)
    
    # legend settings
    patchs = [p1, p2, p3]
    labels = ['Partition 1', 'Partition 2', 'Partition 3']
    
    # plot energy partitions
    for p, part in enumerate(wave_spec.part.values):
        ax = fig.add_subplot(gs[0, p], projection='polar')
        print(wave_spec.sel(part=part).slopeD.values)
        pc = plot_polarfig(mask_comb, K_SPECTRA, wave_spec.sel(part=part).slopeD, N_PHI*2, title, k_min, k_max, 
                          ax=ax, fig=fig, 
                          vmin=vmin, vmax=vmax, disp_partitions=False)
        legend = ax.legend([labels[p]],
                       [labels[p]],
                       loc='best')
        
        #frame = legend.get_frame()
        #frame.set_facecolor('black')
        
        #for text in legend.get_texts():
            #plt.setp(text, color = 'w') 
            
        ax.set_title('Partition {0}'.format(int(p+1)))
        
    # add colorbar
    axc = fig.add_axes([1, 0.35, 0.025, 0.35])
    cbar = fig.colorbar(pc, cax=axc)
    
    return(fig)

def grid(x, y, z):
    "Convert 3 column data to matplotlib grid"
    # avoid interpolation break due to +180/-180º parallel
    x = np.where(x<0, 360-np.abs(x), x)
    xi = np.linspace(min(x), max(x), 100)
    yi = np.linspace(min(y), max(y), 100)
    
    xy = pd.DataFrame({'x':x, 'y':y}).values
    
    X, Y = np.meshgrid(xi, yi)
    Z = griddata(xy, z, (X, Y), )
    
    return X, Y, Z

def addmap(ax):
    'Cartopy map customize'
    ax.patch.set_facecolor('cornflowerblue')
    ax.patch.set_alpha(0.7)
    ax.add_feature(shape_feature_l)

def plot_spectrum(ax, x, y, z, cmap, rmin, rmax, vmax):
    
    # from deg to radians
    x = np.deg2rad(x)
    
    x1 = np.append(x, x[0])
    z1 = np.column_stack((z[:,:], z[:,-1]))
    im = ax.pcolormesh(x1, y, np.sqrt(z1), vmin=0, vmax=vmax, cmap=cmap)  

    ax.set_theta_zero_location('N', offset=0)
    ax.set_theta_direction(-1)
    
    # radius labels in white
    ax.tick_params(labelcolor='k', axis='y')
    ax.set_rlim(rmin, rmax)
    
    return(im)