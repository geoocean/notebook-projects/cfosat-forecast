import os
import os.path as op
import sys

from math import pi
import numpy as np
import pandas as pd
import argparse
import netCDF4 as netcdf
import datetime
import xarray as xr
from datetime import timedelta
from scipy.interpolate import griddata
from matplotlib import pyplot as plt
from matplotlib.patches import Patch

# maths
from math import gamma as gmfunc

# bearing
import great_circle_calculator.great_circle_calculator as gcc
from geopy.distance import geodesic

# dependencies
from .config import *

import warnings
warnings.filterwarnings('ignore')


def get_box(lons, lats, lon_x, lat_x):
    """
    Get nearest satellite-box from the study site
    Usage:
        (box, min_d) = get_box(lons, lats, lon_x, lat_x)
    With:
        :param      lons, lats     :track coordinates   
        :param      lon_x, lat_x   :site coordinates                
        :type       rho            :float
        :returns    (box, min_d)   :coordinates in carthesian
        :rtype      (x,y)   :tuple of floats
    """
    min_d = 1000000
    for i in range(len(lons)):
        dist = geodesic((lat_x, lon_x), (lats[i], lons[i])).kilometers
        if dist < min_d:
            min_d = dist
            box = i
    return box, min_d


def nearest(items, pivot):
    return min(items, key=lambda x: abs(x - pivot))

def SlopeSpectra(dataset, array):
    'Duplicate 180º dataset values to 360º'
    
    dataset = dataset.drop('n_beam_sig0')
    dphi = dataset.dphi
    k_spectra = dataset['k_spectra']
    
    # spectre built with phi = 0° at the north
    dirs = np.arange(0, 360, dphi)
    spec = np.ma.zeros((len(dirs), len(k_spectra)))

    spec = np.vstack([np.ma.masked_where(np.ma.getmask(
                      array.T) == True,
                      array.T),
                      
                      np.ma.masked_where(np.ma.getmask(
                      array.T) == True,
                      array.T)])
    
    # limits inf, limits lambda < 500m
    specm_comb = np.ma.masked_where(spec < -1.e8, spec)
    ds = xr.Dataset(data_vars = dict(slope=(["dp", "k"], specm_comb)), coords = dict(dp=dirs, k=k_spectra.values))
    
    return(ds)

def remove_amb(cdf_i, ds_Slope, ds_mask, dom):
    'remove ambiguity in Dataset masking partitions with angles: directional values in dataset'
    
    angles = ds_Slope.dp.values
    delta = np.unique(np.diff(angles))
    ds_amb = xr.Dataset({"dp":angles})

    for part, ang_part in enumerate(dom):
        if not np.isnan(ang_part):
            mask = ds_mask.isel(partition = part)

            # direction position
            place_angle = np.abs(angles-ang_part).argmin()

            # go through the rows
            lower_row, upper_row = 0, len(angles)-1
            for prow, row in enumerate(angles):
                if (prow < place_angle) & (len(np.unique(mask.sel(dp=row).slope))==1):
                    lower_row = prow
                if (prow > place_angle) & (len(np.unique(mask.sel(dp=row).slope))==1):
                    upper_row = prow
                    break       
                    
            # partitions are melted over the whole 360º sector
            if (lower_row == 0) & (upper_row == len(angles)-1):
                ang_lower = place_angle - int(90/delta)
                ang_upper = place_angle + int(90/delta)
                if ang_lower < 0: ang_lower = int(360/delta) + ang_lower
                if ang_upper > int(360/delta): ang_upper = ang_upper - int(360/delta)
                dsp = ds_mask.isel(partition=part)
                dsp = dsp.where((dsp.dp > angles[ang_lower]) | (dsp.dp < angles[ang_upper]))
                
            else:
                dsp = ds_mask.isel(partition=part).sel(dp = slice(angles[lower_row], angles[upper_row]))
                
            dsp = (ds_Slope * dsp)
            dsp = dsp.assign(partition=[part])
            ds_amb = xr.combine_by_coords([ds_amb, dsp])
        
    return(ds_amb)

def partitions_info(cdf, num_box, nadir_side, beam):
    """
    Print a cell text with the mean partitions parameters
    Usage:
        (cell_text) = partitions_info(lons, lats, lon_x, lat_x)
    With:
        :param      cdf            :netcdf Dataset CFOSAT file 
        :param      nadir_side     :side of the nadir track to analyse (0 : right sides ; 1 :left side)                
        :param      num_box        :float
        :param      beam           :beam choice for spectra (6,8,10,0-combined)
        :returns    cell_text      :mean partitions parameters
        :rtype      ()             :structre
    """

    # Wave parameters of each partition of the combined spectrum (SWH, peak wavelength, peak direction)
    swh_part = cdf.variables['wave_param_part'][0,:,nadir_side,num_box,int((beam-6)/2)]
    dom_wl = cdf.variables['wave_param_part'][1,:,nadir_side,num_box,int((beam-6)/2)]
    dom_azi = cdf.variables['wave_param_part'][2,:,nadir_side,num_box,int((beam-6)/2)]

    swh_part_comb = cdf.variables['wave_param_part_combined'][0,:,nadir_side,num_box]
    dom_wl_comb = cdf.variables['wave_param_part_combined'][1,:,nadir_side,num_box]
    dom_azi_comb = cdf.variables['wave_param_part_combined'][2,:,nadir_side,num_box]

    # for combination of the 3 beams
    if beam == 0 :
        cell_text= pd.DataFrame(
                {
                    'partition': [1,2,3],
                    'SWH(m)':[swh_part_comb[0], swh_part_comb[1], swh_part_comb[2]],
                    'wavelength(m)':[dom_wl_comb[0], dom_wl_comb[1], dom_wl_comb[2]],
                    'direction (°)':[dom_azi_comb[0], dom_azi_comb[1], dom_azi_comb[2]]
                }
            )
    # for one selected beam            
    else :
        cell_text= pd.DataFrame(
                    {
                        'partition': [1,2,3],
                        'SWH(m)':[swh_part[0], swh_part[1], swh_part[2]],
                        'wavelength(m)':[dom_wl[0], dom_wl[1], dom_wl[2]],
                        'direction (°)':[dom_azi[0], dom_azi[1], dom_azi[2]]
                    }
        )

    return(cell_text)

def geoBearing(lon_aux, lat_aux):
    '''
    Get distance and bearing from 2 geographical points
    lon_aux, lat_aux
    Lon, Lat - study point
    '''
    
    if lon_aux < -180: lon_aux = lon_aux+360
    distance = gcc.distance_between_points([lon_aux, lat_aux], [site_lon, site_lat], unit='meters', haversine=True)
    bearing_p1 = gcc.bearing_at_p1([site_lon, site_lat], [lon_aux, lat_aux])
    bearing_p2 = gcc.bearing_at_p2([site_lon, site_lat], [lon_aux, lat_aux])
            
    if bearing_p2 < 0: bearing_p2 = bearing_p2 + 360
    if bearing_p1 < 0: bearing_p1 = bearing_p1 + 360

    return(distance, bearing_p1, bearing_p2)

def propagate(ds):
    '''
    Compute mean wave parameters of directional sector:
    Significant Wave Height - Hs
    Peak Period - T
    Celerity - C
    Travel Time = f(C, distance)
    '''
    rad_ang = np.unique(np.diff(ds.dp)) * (2 * pi) / 360
    dk = np.diff(np.append(0, ds.k))
    
    wl = 2 * np.pi / ds.k
    T = np.sqrt(2 * np.pi * wl / g)
    C = g * T / (2 * np.pi)
    Cg = 0.5 * C
    
    # disipation term - Air Sea Viscosity
    #dens_rate = 0.0013
    #va = 1.4 * 10**(-5)
    #nu = 2 * dens_rate * (1 / (g * Cg)) * ((2 * np.pi / T)**(5/2)) * np.sqrt(2 * va)
    
    # wave characteristics
    Ttime = (ds.distance / Cg) 
    
    H = 4 * np.sqrt(((ds.slope / ds.k) * rad_ang) * (dk))
    
    return(H, T, Cg, Ttime)

def jonswap_spectrum(freqs_recon, dirs_recon, gamma, ds_partitions):
    
    """
    This function calculates a "syntetic" spectrum given the partitions...
    The number of partitions that can be given to the funtion is NOT limited,
    but the example contains just one sea and two swells

    Args:
        partitions (python-dict): These are the partitions, containing hs, tp,
            tm02, dir, spr and gamma, where tm02 and gamma ara optional

    Returns:
        [xr.Dataset]: The function returns an xarray Dataset with variable name
            "efth" and the energy calculated for each frequency and direction
            available in spectra_examples.nc, which is saved in the parent folder
            
    """

    # and iterate for all the partitions
    for partition in ds_partitions.part.values:
        
        # hs, tp, dir, spr and gamma
        ds = ds_partitions.sel(part=partition).squeeze()
        hs, tp, dirr, spr =  ds['hs'].values, ds['tp'].values, ds['dpm'].values, ds['dspr'].values
 
        # calculate sigma value
        sigma = np.where(
            freqs_recon<(1/tp),
            np.ones(len(freqs_recon))*0.07,
            np.ones(len(freqs_recon))*0.09
        ) 
        
        # calculate alpha parameter
        alpha = (0.06238 / (0.23+0.0336*gamma-0.185*(1.9+gamma)**(-1))) * \
            (1.094-0.01915*np.log(gamma))

        # energy in frequencies
        S = alpha * (hs**2) * (tp**(-4)) * (freqs_recon**(-5)) * \
            np.exp(-1.25*(tp*freqs_recon)**(-4)) * \
            gamma**(np.exp((-(tp*freqs_recon-1)**2)/(2*sigma**2)))
        
        # energy in directions
        s = (2/(spr*np.pi/180)**2) - 1 # spectral shape parameter
        D = ((2**(2*s-1))/np.pi)*(gmfunc(s+1)**2/gmfunc(2*s+1)) * \
            np.abs(np.cos(
                (np.deg2rad(dirs_recon)-np.deg2rad(dirr))/2
            ))**(2*s)
        
        # total spectrum
        m = S * D
        m[np.where(np.isnan(m))] = 0
        
        if partition == 0:
            spectrum = m * (np.pi/180)
        else: spectrum += m * (np.pi/180)
    
    return spectrum.to_dataset(name='efth')

def slope2density(ds):
    'Convert 2D mean Slope Spectra to Energy Density Spectrum'
    # Z slope = Z density * k
    
    ds_efth = ds.rename({'slope':'efth', 'dp':'dir', 'k':'freq'})
    ds_efth['efth'] = ds_efth['efth'] #/ ds_efth['freq']
    ds_efth['freq'] =  1 / np.sqrt((4*np.pi**2)/(ds_efth['freq']*g))
    
    return(ds_efth)

def density2slope(ds_efth):
    
    'Convert 2D Energy Density Spectrum to Mean Slope Spectra'
    # Z slope = Z density * k
    
    ds = ds_efth.rename({'efth':'slope', 'dir':'dp', 'freq':'k'})
    ds['k'] =  ds['k']**2 * 4 * np.pi**2 / g
    ds = ds.transpose('dp', 'k')
    
    return(ds)    
    
