#!/usr/bin/env python
# -*- coding: utf-8 -*-
# pip
import os
import os.path as op

import datetime
import numpy as np

# cartopy maps
import cartopy.crs as ccrs
from cartopy.io.shapereader import Reader
from cartopy.feature import ShapelyFeature
from shapely.ops import unary_union
from shapely.prepared import prep
import shapely.geometry as sgeom

# local paths
p_data = op.join(os.getcwd(), '..', 'data')
p_output = op.join(p_data, '..', 'output')

file_timelag = op.join(p_data, "Trace_TimeStamps.kml")
file_tracks = op.join(p_data, "cfosat_cycles_t0_v2.3.txt")

# datarmor data paths (SWIM satellite, WW3 forecast)
#sat_cfosat = r'/home/datawork-cersat-public/provider/cnes/satellite/l2/cfosat/swim/swi_l2____/2021' 
sat_cfosat = r'/home/datawork-cersat-public/provider/cnes/satellite/l2/cfosat/swim/swi_l2____/op05/5.1.2/2021'
mfawm = r'/home/datawork-cersat-public/project/cfosat/data/ancillary/mfwam'

ww3_path = '/home/datawork-WW3/FORECAST/GLOBMULTI/GLOB-30M/FIELD_NC/best_estimate/2021'        
ww3_spectra_path = '/home/datawork-WW3/FORECAST/GLOBMULTI/GLOB-30M/SPEC_NC/best_estimate_hourly/2021'
ww3_spectra_proccessed = r'/home/ref-cfosat-public/datasets/colocations/swim/model/ww3/swi_l2____/2021' 

# GSHHS path to shorelines (full and low resolution)
fname_f = r'/home3/datahome/aricondo/Documents/cfosat-forecast/data/GSHHS_f_L1.shp'
#fname_l = r'/home3/datahome/aricondo/Documents/cfosat-forecast/data/GSHHS_l_L1.shp'

fname_l = r'/media/administrador/DiscoHD/Documentos/notebook_projects/cfosat-forecast/data/GSHHS_l_L1.shp'

# low resolution shapefile
shape_feature_l = ShapelyFeature(Reader(fname_l).geometries(),
                                 ccrs.PlateCarree(),
                                 facecolor='beige', alpha=1, edgecolor='black')
# full resolution shapefile
#shape_feature_f = ShapelyFeature(Reader(fname_f).geometries(),
#                                 ccrs.PlateCarree(),
#                                 facecolor='beige', alpha=1, edgecolor='black')

land_geom = unary_union(list(Reader(fname_l).geometries()))
land = prep(land_geom)

# forecast days
site = 'Samoa Island'
#forecast_day = datetime.datetime.now()
t0 = datetime.datetime(2009, 1, 1)
#forecast_day = datetime.datetime(2021, 2, 23, 0, 0, 0, 0) # Tropical storm

'''
August 10 – August 19, Category 4 Hurricane

Not MFWAN files:050, 095, 

049 - coloc_mfwam_2021 02 18
094 - coloc_mfwam_2021 04 04
'''
'Category 4 Hurricane'
forecast_day = datetime.datetime(2021, 1, 30, 0, 0, 0)
n_days = 10

# default forecast location
site_lon, site_lat = -172, -13.8
lon1_P, lon2_P = site_lon-50, site_lon+85
lat1_P, lat2_P = site_lat-65, site_lat+75

# global vars
g = 9.806

# --------------------------------------------------------------
# SWIM config parameters

# beam choice for spectra : 6=> beam 6°; 8=> beam 8°; 10=> beam 10° ; 0 => combined (combination of the 3 beams)
beam = 10
n_beam_l2 = int((beam-6)/2)

'wave wavelength visualization range'
# min wavelength (m), instrument specification :70m
min_wavelength = 70

# max wavelength (m), instrument specification :500m
max_wavelength = 500 

# --------------------------------------------------------------
# SPECTRA parameters
angles = np.arange(0, 360, 5)
beam = 10
delta_dir = 5
flag_hs = 0.7

variables = ['time_spec_l2','lat_nadir_l2', 'lon_spec_l2', 'lat_spec_l2', 'k_spectra', 'phi_vector', 'n_beam_sig0', 'swh_ecmwf', \
     'u10_ecmwf', 'v10_ecmwf', 'wave_param_combined', 'wave_param', 'mask', 'p_combined', 'wave_param_part_combined', \
     'mask_combined', 'pp_mean']
