"""
Module attrs
"""

__version__     = '0.0.1'
__author__      = 'Surf And Surge Research Group (UC)'
__contact__     = ''
__url__         = ''
__description__ = ''
__keywords__    = ''

from . import spectra
from . import plot_spectra
from . import io
from . import config