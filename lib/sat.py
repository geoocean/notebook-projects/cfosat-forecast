#!/usr/bin/env python
# -*- coding: utf-8 -*-


import os
import os.path as op
import glob
import sys

# arrays
import numpy as np
import pandas as pd
import xarray as xr
import datetime
import netCDF4 as netcdf

# dependencies
from lib.config import *

'''Library to process the satellite passes over the area'''

def isin_estela(lon, lat, estela_mask):
    'Return boolean var True False if is contained in the ESTELA'
    val = estela_mask.isel(
        longitude=np.argmin(np.abs(estela_mask.longitude.values-lon)),
        latitude=np.argmin(np.abs(estela_mask.latitude.values-lat)),
    ).values
    
    if val == 1: return(True)
    else: return(False)
    
def lonlat_track(fneed, estela_mask):
    '''From selected files over the area, extract lon-lat coordinates of bith sides of the track
    fneed: list of files over the area
    '''
    
    # Get nadir box time
    t0 = datetime.datetime(2009,1,1)

    ds_data = xr.Dataset({'file':[]})
    files, st_nbox, ed_nbox, date_stbox, date_edbox = [], [], [], [], []    # start nbox, end nbox
    
    for pfile, file in enumerate(fneed['name'].values):
        sys.stdout.write('\r File number: {0}/{1}'.format(pfile, len(fneed['name'].values)))
        sys.stdout.flush()

        # SWIM L2 NetCDF file full name
        SWIM_L2_NC_file_name = file

        # NetCDF file opening and reading
        cdf = netcdf.Dataset(glob.glob(op.join(sat_cfosat,  '*', SWIM_L2_NC_file_name))[0])
        time = cdf.variables['time_spec_l2'][:]

        # SWIM lat-lon coordinates
        try:
            # nadir coordinates
            lat_nadir = cdf.variables['lat_nadir_l2'][:].data[1:]
            lon_nadir = cdf.variables['lon_nadir_l2'][:].data[1:]

            # lat_spec_l2: Mean latitude of 2D spectrum coverage area (n_posneg, n_box)
            lat_spec = cdf.variables['lat_spec_l2'][:]
            lon_spec = cdf.variables['lon_spec_l2'][:]

        except Exception:
            print('Nadir coordinates of boxes unavailable. Using lon_l2, lat_l2 for posneg=0 to plot trace instead.')

        # select box numbers that are within the area
        nboxs = []
        lon_nadir = np.where(lon_nadir < 0, lon_nadir+360, lon_nadir)
        for pbox in range(len(lon_nadir)):
            if isin_estela(lon_nadir[pbox], lat_nadir[pbox], estela_mask):
                nboxs.append(pbox)

                for nposg in [0,1]:
                    date_time = [[pd.to_datetime((t0 + datetime.timedelta(seconds=float(time[nposg, pbox, 0]))).strftime('%Y-%m-%dT%H:%M:%S'))]]
                    ds = xr.Dataset(
                         {
                             "longitude": (["file", "nbox", "posneg"], [[[lon_spec[nposg, pbox]]]]),
                             "latitude": (["file", "nbox", "posneg"], [[[lat_spec[nposg, pbox]]]]),
                             "time": (["file", "nbox", "posneg"], [date_time])
                         },
                         coords={
                             "file": [file],
                             "nbox": [pbox],
                             "posneg": [nposg]
                         })
                    ds_data = xr.merge([ds_data, ds])
           
    return(ds_data)


def grid_arrival(df, range_dir, range_time):
    
    gdf = df[['Hs', 'bearing1']]
    gdf['date'] = pd.to_datetime(df.date)

    bins = np.arange(0,360+range_dir,range_dir)
    time_bins = np.arange(df.date.min(), df.date.max()+datetime.timedelta(hours=range_time), datetime.timedelta(hours=range_time))
    gdf['box_bearing'] = [np.where(bins<=i)[0][-1]*range_dir for i in gdf.bearing1]
    gdf['box_time'] = [np.where(time_bins<=i)[0][-1] for i in gdf.date]

    date_group = gdf.groupby(['box_time','box_bearing'], as_index=False).mean()
    date_group['time'] = time_bins[0] + date_group['box_time'] * datetime.timedelta(hours=range_time)

    time = np.arange(date_group.time.min(), 
                     date_group.time.max() + datetime.timedelta(hours=range_time), 
                     datetime.timedelta(hours=range_time))

    X, Y = np.meshgrid(time, bins)
    cross_dt = pd.DataFrame(
        {
            'time':np.reshape(X, (np.shape(X)[0]*np.shape(X)[1])),
            'box_bearing':np.reshape(Y, (np.shape(Y)[0]*np.shape(Y)[1]))
        }
    )
    reg_data = date_group.merge(cross_dt, how='outer')[['box_bearing', 'time', 'Hs']].sort_values(['time', 'box_bearing'])

    X, Y = np.meshgrid(np.unique(reg_data.box_bearing), np.unique(reg_data.time))
    Z = np.reshape(reg_data.Hs.values, (np.shape(X)[0], np.shape(X)[1]))
    
    return(X, Y, Z, reg_data)