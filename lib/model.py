#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import os.path as op
import sys
import glob

import numpy as np
import pandas as pd
import datetime
import xarray as xr
from datetime import timedelta

# matplotlib
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import matplotlib.gridspec as gridspec

import warnings
warnings.filterwarnings('ignore')

# dependencies
from lib.config import *

def hindcast_ndays(array_vars):
    'Select last ndays files from WW3 global grid'
    
    files = glob.glob(op.join(ww3_path, '*'))
    name, time = [], []
    ds_ww3 = xr.Dataset()

    for p, i in enumerate(files):
        date = files[p].split('WW3-GLOB-30M_')[1][:-3]
        date = np.datetime64(pd.to_datetime(date))
        if (date > forecast_day - datetime.timedelta(days=n_days)) & (date <= forecast_day):
            dst = xr.open_dataset(i)[array_vars]
            ds_ww3 = xr.merge([ds_ww3, dst])
            name.append(i)
            time.append(date)

    df_ww3 = pd.DataFrame(
        {
            'name':name,
            'date':time
        }
    ) 
    return(df_ww3)

def spec_st_ndays(st_name):
    'Select following days files from WW3 spectral station'
    
    # input spectral file
    path = op.join(ww3_spectra_path, st_name)
    files = glob.glob(op.join(path, '*'))

    ds_ww3 = xr.Dataset()

    for p, i in enumerate(files):
        date = files[p].split('MARC_WW3-GLOB-30M-{0}_'.format(st_name))[1][:-8]
        date = np.datetime64(pd.to_datetime(date))
        if (date > forecast_day):
            dst = xr.open_dataset(i)
            ds_ww3 = xr.merge([ds_ww3, dst])
    
    return(ds_ww3)


def plot_spectrum(fig, ax, x, y, z, vmin, vmax):
    x1 = np.append(x, x[0])
    z1 = np.column_stack((z[:,:], z[:,-1]))
    pc = ax.pcolormesh(x1, y, z1, vmin=vmin, vmax=vmax, cmap='rainbow')  
    ax.set_theta_zero_location('N', offset=0)
    ax.set_theta_direction(-1)
    ax.set_ylim([0, 0.3])
    ax.tick_params(axis='y', colors='w')
    ax.grid(linewidth=0.8, color='silver', alpha=0.5, linestyle='--')

    return pc

# wave to direction - wave from direction
def fix_dir(base_dirs):
    'fix ww3 direction for wavespectra (to -from)'
    new_dirs = base_dirs + 180
    new_dirs[np.where(new_dirs>=360)] = new_dirs[np.where(new_dirs>=360)] - 360

    return new_dirs

def animation_val(spec, ds, dataset, fore_time, var, nframes, figsize):
    
    matplotlib.rcParams['animation.embed_limit'] = 2**128

    # plot polar plot
    fig = plt.figure(figsize=figsize)
    gs = gridspec.GridSpec(1,2)

    ax = fig.add_subplot(gs[0,0], projection='polar')
    ax1 = fig.add_subplot(gs[0,1], projection='polar')

    global pc, pc1
    pc = ax.scatter([], [])
    pc1 = ax.scatter([], [])

    # loop for time from forecast day
    def update(frame):

        global pc, pc1
        #for tp in pc.collections: tp.remove() # remove the existing contours
        #for tp in pc1.collections: tp.remove() # remove the existing contours
        pc.remove()
        pc1.remove()

        DF = pd.DataFrame()
        for pfile, file in enumerate(dataset.file.values):

            ds_par = ds.sel(file=file).expand_dims('file')
            ds_iT = dataset.sel(file=file).expand_dims('file')

            dataframe = xr.merge([ds_par, ds_iT]).to_dataframe().dropna()
            dataframe['date'] = pd.to_timedelta(dataframe['Ttime'], unit='s') + dataframe['time']
            DF = DF.append(dataframe.loc[(dataframe.date >= fore_time[frame]) & (dataframe.date < fore_time[frame+1])])

        # count bins
        bins_bear = np.sort(spec.direction.values) #np.arange(0, 360+15, 15)
        bins_k = np.sort(spec.frequency.values) #np.linspace(0.01, 0.30, 16)
        matrix = np.zeros((len(bins_bear), len(bins_k)))

        # CFOSAT
        # Energy bins selected -> polar plot of energy(freq, dir)
        Efth_H = DF.reset_index()[['bearing1', 'k', var]]
        # Convert k -> frequency
        Efth_H['freq'] = 1/np.sqrt(4*np.pi**2/(Efth_H['k']*g))
        # Convert H - Efth
        df, do = np.diff(bins_k)[0], np.diff(bins_bear)[0]
        Efth_H['Efth'] = (Efth_H[var]**2/16)/(df*do*2*np.pi/360)

        for pr, row in enumerate(bins_bear[:-1]):
            for pc, col in enumerate(bins_k[:-1]):
                subset = Efth_H.loc[(Efth_H.bearing1 >= row) & (Efth_H.bearing1 < bins_bear[pr+1]) & (Efth_H.freq >= col) & (Efth_H.freq < bins_k[pc+1])]
                subset = subset.loc[subset.Efth != 0]
                matrix[pr, pc] = np.nanmean(subset.Efth.values)
                if np.isnan(matrix[pr, pc]): matrix[pr, pc] = 0

        # WW3
        spec_i = spec.isel(time=np.argsort(np.abs(spec.time.values-np.datetime64(fore_time[frame])))[0]).squeeze()
        spec_i['direction'] = fix_dir(spec_i.direction.values)
        vmax = np.nanmax([np.nanmax(spec_i.efth.values), np.nanmax(matrix.T)])

        pc = plot_spectrum(fig, ax, np.deg2rad(spec_i.direction.values), spec_i.frequency.values, spec_i.efth.values,
                                 0, vmax)
        pc1 = plot_spectrum(fig, ax1, np.deg2rad(bins_bear), bins_k, matrix.T,
                                  0, vmax)

        if frame == 0:

            # axes attributes
            ax.set_title('WW3 \nDate {0}'.format(np.datetime_as_string(spec_i.time.values, unit='D')))
            ax1.set_title('CFOSAT \nDate {0}'.format(fore_time[frame].strftime("%Y-%m-%d"), unit='D'))

            axc = fig.add_axes([0.95, 0.3, 0.03, 0.35])
            fig.add_axes(axc)
            cbar = fig.colorbar(pc, cax=axc)
            cbar.set_label('Sf')
            cbar.outline.set_edgecolor('white')

        return pc

    ani = FuncAnimation(fig, update, frames=nframes, # range(len(fore_time)-1),
                    interval=500,
                    repeat=False,
                    blit=False)
    plt.close()
    
    return ani



