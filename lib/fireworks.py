#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import os.path as op
import sys
import glob

import numpy as np
import pandas as pd
import netCDF4 as netcdf
import datetime
import xarray as xr
from math import pi
import math
from datetime import timedelta
from datetime import datetime

# shapefiles reader
import cartopy.io.shapereader as shpreader
import shapely.geometry as sgeom
from shapely.ops import unary_union
from shapely.prepared import prep

import gc
import great_circle_calculator.great_circle_calculator as gcc

import warnings
warnings.filterwarnings('ignore')

# dependencies
from lib import spectra, io
from lib.config import *



def rotate_ang(angle):
    """calculate the oppposite direction to the partition direction
    
    Args:
        float: propagation bearing in degreed

    Returns:
        float: back-propagation bearing in degrees
        
    """
    
    if angle < 180: angle = angle + 180
    else: angle = angle - 180
        
    return(angle)


def get_mask_land(lon, lat):
    """
    Function to determine if the point is land
    return: bool
    
    Args:
    
    """
    return(land.contains(sgeom.Point(lon, lat)))

def get_date(T, date, distance):
    
    C = g*T/(2*np.pi)
    Ttime = (distance/C)
    
    return(date - np.timedelta64(int(Ttime),'s'))

def cut_wind(ds_ww3, lon, lat, date, T):
    """
    Apply a filter to discard observations with a wind speed < 2/3 Umin
    """
    
    # Values of threshold wind
    Umin = 0.12 * g * T
    Wcut = (2/3) * Umin
    
    # WW3 wind
    W_model = ds_ww3.isel(time=np.argmin(np.abs(ds_ww3.time.values - date)),
                longitude=np.argmin(np.abs(ds_ww3.longitude.values - lon)),
                latitude=np.argmin(np.abs(ds_ww3.latitude.values - lat)),
               ).wind.values
    
    if W_model >= Wcut:
        return True
    else:
        return False
    
    
def calculate_greate_circle(ds_ww3, lon, lat, T, date, back_angle):
    """
    Iterative great circle calculation given starting point and bearing
    and distance between points (meters)
    
    Args:


    Returns:
        float or array: longitudes and latitudes of points along the bearing
    """
    
    dd_distance = 100000 # 100 km
    distance = 0
    
    mask = get_mask_land(lon, lat)
    
    lon_gc, lat_gc, date_gc = [], [], []
    while mask == False:
        
        # calculate next point along the greatcircle
        lon2, lat2 = gcc.point_given_start_and_bearing([lon, lat], back_angle, distance, unit='meters') 
        
        # date value to retracked points f = (Tp, distance)
        date2 = get_date(T, date, distance)
        
        # filter point depending on the wind threshold
        filt_wind = cut_wind(ds_ww3, lon2, lat2, date2, T)
        
        # storage coordinates
        if filt_wind:
            lon_gc.append(lon2)
            lat_gc.append(lat2)
            date_gc.append(date2)
        
        # bool land
        mask = get_mask_land(lon2, lat2)
        
        # max Time
        diff_time = date2 - date
        if diff_time > np.timedelta64(14, 'D'): mask = True
        
        # next point spaced dd_distance meters
        distance = distance + dd_distance
        
    if not date_gc == []:
        gcll = pd.DataFrame({'lon': lon_gc, 'lat': lat_gc, 'date':pd.to_datetime(np.concatenate(date_gc))})
        
    else:
        gcll = pd.DataFrame({'lon':[], 'lat':[], 'date':pd.to_datetime([])})
    return(gcll)


def calculate_fireworks(ds, ds_ww3, folder):
    """ Aggregate into xr Dataset the lon lat coordinates of the greatcircles calculations
    
    Args:
        xr Dataset: CFOSAT partitions information, file, nbox, nadir_side
        xr Dataset: WW3 uwind, vwind, wind
    
    Returns:
        xr Dataset: great circles calculations
    """
    dts = xr.Dataset({'case':[]})
    rout = 0
    idc = 0
    for prow, row in enumerate(ds.index):
        sub_ds = ds.loc[[row]]

        # stating point
        lon, lat = sub_ds.longitude.values, sub_ds.latitude.values

        # course: back direction of partition
        fore_angle = sub_ds.dir.values
        #back_angle = rotate_ang(fore_angle)
        
        # obervation date
        T = sub_ds.Tp.values
        date = sub_ds.time.values
        
        # lon, lat coordinates of the points along the gc
        gcll = calculate_greate_circle(ds_ww3, lon, lat, T, date, fore_angle)
        cor = gcll.set_index('date').to_xarray().assign_coords(case=prow)
        dts = xr.concat([dts, cor], dim='case')
        idc = idc + 1
        
        if idc == 100:
            dts.to_netcdf(op.join(p_output, folder, 'Gc_{0}'.format(rout)))
            rout = rout + 1
            idc = 0
            dts = xr.Dataset({'case':[]})
            
        #gcll.to_pickle(op.join(p_output, 'process_GreatCircles', 'Gc_{0}_{1}{2}.pkl'.format(
        #    sub_ds.nbox.values[0], 
        #    sub_ds.posneg.values[0], 
        #    sub_ds.file.values[0][:-3])))
    #return(dts)

                  
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    