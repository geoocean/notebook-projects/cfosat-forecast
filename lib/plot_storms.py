#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import os.path as op
import sys

from math import pi
import numpy as np
import argparse
import netCDF4 as netcdf
import datetime
import xarray as xr
import pandas as pd

import matplotlib
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
from matplotlib.animation import FuncAnimation
import matplotlib.dates as mdates

# cartopy maps
import cartopy.crs as ccrs
from cartopy.io.shapereader import Reader
from cartopy.feature import ShapelyFeature

from lib import plot_sat
from .config import *

def colormap(lim_up, lim_down):
    """
    Custom colormap from 2 user python colorbars
    """
    
    colors1, colors2 = 'jet', 'jet'
    bottom = cm.get_cmap(colors2, lim_down)
    top = cm.get_cmap(colors1, lim_up)
    newcolors = np.vstack((bottom(np.linspace(0, 0.8, lim_down)),
                       top(np.linspace(0.1, 1, lim_up))))
    
    # linear increasing alpha
    newcolors[:,-1] = np.linspace(0, 1, len(newcolors))
    
    return(ListedColormap(newcolors))

def animate_wind_storms(dst, ds_data, ds_ww3, Tmax, max_density, time, cmap, xedges, yedges):
    
    matplotlib.rcParams['animation.embed_limit'] = 2**128
    fig = plt.figure(figsize=(15,8))
    ax = fig.add_subplot(111, projection=ccrs.PlateCarree(central_longitude=180))
    ax.add_feature(shape_feature_l)
    fig.subplots_adjust(left=0, bottom=0, right=0.9, top=1, wspace=None, hspace=None)
    
    global im, sc, cs
    sc = ax.scatter([], [])
    cs = ax.contour(np.zeros((2,2)))
    im = ax.contour(np.zeros((2,2)))
    
    def update(frame):

        global im, sc, cs
        for tp in im.collections: tp.remove() # remove the existing contours
        for tp in cs.collections: tp.remove() # remove the existing contours
        sc.remove()
        
        # slice winds
        wind = ds_ww3.sel(time=ds_ww3.time[np.argsort(np.abs(ds_ww3.time-time[frame]))[0]])
        cs = ax.contour(wind.longitude, wind.latitude, wind.wind, cmap='coolwarm',
                        levels = np.linspace(0, np.max(ds_ww3.wind)/2, 10), alpha=0.8,
                        transform=ccrs.PlateCarree(), zorder=1)

        # slice observations Hs
        df_hs = ds_data.loc[(ds_data.time > time[frame]) & (ds_data.time < time[frame+1])]
        
        sc = ax.scatter(df_hs.longitude, df_hs.latitude, 
                        c='r', s=df_hs.hs**2, label='CFOSAT Hs scale (m)', transform=ccrs.PlateCarree(), zorder=2)

        # slice back-propagate trayectories
        ds_delta = dst.sel(date=slice(time[frame], time[frame+1]))
        dataframe = ds_delta.to_dataframe().reset_index().dropna()

        # histogram 2D
        Z, xed, yed = np.histogram2d(dataframe.lon, dataframe.lat, bins=(xedges, yedges))

        im = ax.contourf(xed[:-1], yed[:-1], Z.T, cmap=cmap, levels = np.linspace(0, max_density, 20),
                         transform=ccrs.PlateCarree(), zorder=3)
        ax.set_title('Storms Location \n {0}'.format(pd.to_datetime(str(time[frame])).strftime('%Y/%m/%d-%m'),
                                                             fontsize=65, fontweight="bold", pad=13))
        
        # plt colorbar
        if frame == 0:
            axc, loc = plot_sat.add_colorbar(ax)
            fig.add_axes(axc)
            ax.set_extent([-180, 180, -90, 90])
            fig.colorbar(im, cax=axc, label='Observations density')
            ax.legend(loc='upper right') 
            
            #divider = make_axes_locatable(ax)
            #axt = divider.new_hor('right', size='5%', pad=0.3)
            #axt = divider.append_axes("left", size="50%", pad=0.5)
            #fig.add_axes(axt)
            #axt = fig.add_axes([0.1, 0.1, 0.03, 0.8])
            #axt = divider.new_horizontal(size=0.5, pad=0.1, axes_class=plt.Axes)
            #fig.colorbar(cs, cax=axt, label='Wind speed (m/s)')

        return im,

    ani = FuncAnimation(fig, update, frames=range(len(time)-1),
                        interval=500,
                        repeat=False,
                        blit=False)
    plt.close()
    
    return ani

def animate_storms(dst, ds_data, ds_ww3, Tmax, max_density, time, cmap, xedges, yedges):
    
    matplotlib.rcParams['animation.embed_limit'] = 2**128
    fig = plt.figure(figsize=(15,8))
    ax = fig.add_subplot(111, projection=ccrs.PlateCarree(central_longitude=180))
    ax.add_feature(shape_feature_l)
    fig.subplots_adjust(left=0, bottom=0, right=0.9, top=1, wspace=None, hspace=None)
    
    global im, sc
    sc = ax.scatter([], [])
    im = ax.contour(np.zeros((2,2)))
    
    def update(frame):

        global im, sc
        for tp in im.collections: tp.remove() # remove the existing contours
        sc.remove()
        
        # slice observations Hs
        df_hs = ds_data.loc[(ds_data.time > time[frame]) & (ds_data.time < time[frame+1])]
        
        sc = ax.scatter(df_hs.longitude, df_hs.latitude, 
                        c='r', s=df_hs.hs**2, label='CFOSAT Hs scale (m)', transform=ccrs.PlateCarree(), zorder=2)

        # slice back-propagate trayectories
        ds_delta = dst.sel(date=slice(time[frame], time[frame+1]))
        dataframe = ds_delta.to_dataframe().reset_index().dropna()

        # histogram 2D
        Z, xed, yed = np.histogram2d(dataframe.lon, dataframe.lat, bins=(xedges, yedges))

        im = ax.contourf(xed[:-1], yed[:-1], Z.T, cmap=cmap, levels = np.linspace(0, max_density, 20),
                         transform=ccrs.PlateCarree(), zorder=3)
        ax.set_title('Storms Location \n {0}'.format(pd.to_datetime(str(time[frame])).strftime('%Y/%m/%d-%m'),
                                                             fontsize=65, fontweight="bold", pad=13))
        
        # plt colorbar
        if frame == 0:
            axc, loc = plot_sat.add_colorbar(ax)
            fig.add_axes(axc)
            ax.set_extent([-180, 180, -90, 90])
            fig.colorbar(im, cax=axc, label='Observations density')
            ax.legend(loc='upper right') 
            
        return im,

    ani = FuncAnimation(fig, update, frames=range(len(time)-1),
                        interval=500,
                        repeat=False,
                        blit=False)
    plt.close()
    
    return ani

def animate_backtracks(dst, ds_data, ds_ww3, Tmax, max_density, time, cmap, xedges, yedges):
    
    matplotlib.rcParams['animation.embed_limit'] = 2**128
    fig = plt.figure(figsize=(15,8))
    ax = fig.add_subplot(111, projection=ccrs.PlateCarree(central_longitude=180))
    ax.add_feature(shape_feature_l)
    fig.subplots_adjust(left=0, bottom=0, right=0.9, top=1, wspace=None, hspace=None)
    
    cmap = plt.cm.rainbow  
    vmin, vmax = mdates.date2num(dst.date.min()), mdates.date2num(dst.date.max())
    
    global im, sc, cs
    sc = ax.scatter([], [])
    cs = ax.contour(np.zeros((2,2)))
    im = ax.scatter([], [])
    
    def update(frame):

        global im, sc, cs
        for tp in cs.collections: tp.remove() # remove the existing contours
        im.remove()
        sc.remove()
        
        # slice winds
        wind = ds_ww3.sel(time=ds_ww3.time[np.argsort(np.abs(ds_ww3.time-time[frame]))[0]])
        cs = ax.contour(wind.longitude, wind.latitude, wind.wind, cmap='coolwarm',
                        levels = np.linspace(0, np.max(ds_ww3.wind)/2, 10), alpha=0.8,
                        transform=ccrs.PlateCarree(), zorder=1)

        # slice observations Hs
        df_hs = ds_data.loc[(ds_data.time > time[frame]) & (ds_data.time < time[frame+1])]
        
        sc = ax.scatter(df_hs.longitude, df_hs.latitude, 
                        c='r', s=4, label='CFOSAT Hs scale (m)', transform=ccrs.PlateCarree(), zorder=2)

        # slice back-propagate trayectories
        ds_delta = dst.sel(date=slice(time[frame], time[frame+1]))
        dataframe = ds_delta.to_dataframe().reset_index().dropna()
        
        # scatter back-propagated trayectories
        t = mdates.date2num(dataframe.date)
        im = ax.scatter(dataframe.lon, dataframe.lat, c=t, vmin=vmin, vmax=vmax,
                s=5, cmap=cmap, transform=ccrs.PlateCarree(), zorder=3)
        
        ax.set_title('Fireworks \n {0}'.format(pd.to_datetime(str(time[frame])).strftime('%Y/%m/%d-%m'),
                                                             fontsize=55, fontweight="bold", pad=13))
        
        # plt colorbar
        if frame == 0:
            ax.set_extent([-180, 180, -90, 90])
            axc, loc = plot_sat.add_colorbar(ax)
            fig.add_axes(axc)
            cb = fig.colorbar(im, cax=axc, ticks=loc,
                         format=mdates.AutoDateFormatter(loc),)# extend='min'
            cb.set_label(label='Travel Date', size='large', weight='bold')
            ax.legend(loc='upper right')  

        return im,

    ani = FuncAnimation(fig, update, frames=range(len(time)-1),
                        interval=500,
                        repeat=False,
                        blit=False)
    plt.close()
    
    return ani